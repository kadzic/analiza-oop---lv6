﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Znanstveni_kalkic
{
    public partial class Form1 : Form
    {
        double rezultat;
        string izvodenjeOperacije = "";
        bool izvrsenaOperacija = false;
        public Form1()
        {
            InitializeComponent();
        }

        private void button_click(object sender, EventArgs e)
        {

            if((textBox_rezultat.Text == "0") || (izvrsenaOperacija))
            {
                textBox_rezultat.Clear();
            }

            izvrsenaOperacija = false;
            Button gumb = (Button)sender;
            textBox_rezultat.Text = textBox_rezultat.Text + gumb.Text;
        }

        private void operator_click(object sender, EventArgs e)
        {
            Button gumb = (Button)sender;
            izvodenjeOperacije = gumb.Text;
            rezultat = double.Parse(textBox_rezultat.Text);
            label_ispis.Text = rezultat + " " + izvodenjeOperacije;
            izvrsenaOperacija = true;
        }

        private void button_jednako(object sender, EventArgs e)
        {
            switch(izvodenjeOperacije)
            {
                case "+":
                    textBox_rezultat.Text = (rezultat + double.Parse(textBox_rezultat.Text)).ToString();
                    break;
                case "-":
                    textBox_rezultat.Text = (rezultat - double.Parse(textBox_rezultat.Text)).ToString();
                    break;
                case "*":
                    textBox_rezultat.Text = (rezultat * double.Parse(textBox_rezultat.Text)).ToString();
                    break;
                case "/":
                    textBox_rezultat.Text = (rezultat / double.Parse(textBox_rezultat.Text)).ToString();
                    break;
                default:
                    break;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            textBox_rezultat.Clear();
        }

        private void btn_sin_Click(object sender, EventArgs e)
        {
            double sinus = double.Parse(textBox_rezultat.Text);
            label_ispis.Text = System.Convert.ToString("sin" + "(" + (double.Parse(textBox_rezultat.Text)) + ")");
            sinus = Math.Sin(sinus);
            textBox_rezultat.Text = System.Convert.ToString(sinus);
        }

        private void btn_cos_Click(object sender, EventArgs e)
        {
            double kosinus = double.Parse(textBox_rezultat.Text);
            label_ispis.Text = System.Convert.ToString("cos" + "(" + (double.Parse(textBox_rezultat.Text)) + ")");
            kosinus = Math.Cos(kosinus);
            textBox_rezultat.Text = System.Convert.ToString(kosinus);
        }

        private void btn_tg_Click(object sender, EventArgs e)
        {
            double tangens = double.Parse(textBox_rezultat.Text);
            label_ispis.Text = System.Convert.ToString("tg" + "(" + (double.Parse(textBox_rezultat.Text)) + ")");
            tangens = Math.Tan(tangens);
            textBox_rezultat.Text = System.Convert.ToString(tangens);
        }

        private void btn_sqrt_Click(object sender, EventArgs e)
        {
            double korijen = double.Parse(textBox_rezultat.Text);
            label_ispis.Text = System.Convert.ToString("sqrt" + "(" + (double.Parse(textBox_rezultat.Text)) + ")");
            korijen = Math.Sqrt(korijen);
            textBox_rezultat.Text = System.Convert.ToString(korijen);
        }

        private void btn_log_Click(object sender, EventArgs e)
        {
            double ilog = double.Parse(textBox_rezultat.Text);
            label_ispis.Text = System.Convert.ToString("log" + "(" + (double.Parse(textBox_rezultat.Text)) + ")");
            ilog = Math.Log10(ilog);
            textBox_rezultat.Text = System.Convert.ToString(ilog);
        }
    }
}
