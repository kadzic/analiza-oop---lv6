﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Igra_vjesala
{
    public partial class Form1 : Form
    {
        int i;
        int pokusaji = 15;
        string target;
        public static string path;
        char pogodi;
        bool pogodak = false;
        
        public Form1()
        {
            InitializeComponent();
        }

        private void btn_lista_Click(object sender, EventArgs e)
        {
            OpenFileDialog file = new OpenFileDialog();
            if(file.ShowDialog() == DialogResult.OK)
            {
                path = file.FileName;
            }
            StreamReader sr = new StreamReader(path);
            while (sr.ReadLine() != null)
            {
                i++;
            }
            sr.Dispose();
            sr = new StreamReader(path);
            Random rand = new Random();
            for(int k = 0; k < rand.Next(1,i-1); k++)
            {
                target = sr.ReadLine();
            }
            sr.Dispose();
            MessageBox.Show("Random riječ: " + target);
            Postavljanje();
        }

        private void Postavljanje()
        {
            label_preostalo_pokusaja_broj.Text = " ";
            pokusaji = 15;
            label_preostalo_pokusaja_broj.Text = label_preostalo_pokusaja_broj.Text + pokusaji;
            for(i = 0; i < target.Length; i++)
            {
                label_rijeci.Text = label_rijeci.Text.Insert(i, "*");
            }
        }

        private void btn_Quit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btn_Pogodi_Click(object sender, EventArgs e)
        {
            pogodi = textBox.Text[0];
            for (i = 0; i < target.Length; i++)
            {
                if(char.ToUpper(target[i]) == char.ToUpper(pogodi))
                {
                    pogodak = true;
                    label_rijeci.Text = label_rijeci.Text.Remove(i, 1);
                    label_rijeci.Text = label_rijeci.Text.Insert(i, pogodi.ToString());
                    label_preostalo_pokusaja_broj.Text = "";
                    label_preostalo_pokusaja_broj.Text = label_preostalo_pokusaja_broj.Text + pokusaji;
                    if (pokusaji <= 0)
                    {
                        IzgubljenaIgra();
                    }
                }
            }
            if((label_rijeci.Text.ToUpper() == target.ToUpper()))
            {
                DobitnaIgra(); 
                label_preostalo_pokusaja_broj.Text = "";
                label_preostalo_pokusaja_broj.Text = label_preostalo_pokusaja_broj.Text + pokusaji;
                if (pokusaji <= 0)
                {
                    IzgubljenaIgra();
                }
            }
            if (!pogodak)
            {
                pokusaji--;
                label_preostalo_pokusaja_broj.Text = "";
                label_preostalo_pokusaja_broj.Text = label_preostalo_pokusaja_broj.Text + pokusaji;
                if (pokusaji <= 0)
                {
                    IzgubljenaIgra();
                }
            }
            pogodak = false;
        }

        private void IzgubljenaIgra()
        {
            MessageBox.Show("Izgubili ste!");
            textBox.Clear();
        }
        private void DobitnaIgra()
        {
            MessageBox.Show("Pobjedili ste!");
            textBox.Clear();
        }

    }
}
